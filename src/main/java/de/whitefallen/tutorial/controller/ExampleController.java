package de.whitefallen.tutorial.controller;


import de.whitefallen.tutorial.entity.h2.H2Album;

import de.whitefallen.tutorial.entity.mongo.MongoAlbum;
import de.whitefallen.tutorial.repository.h2.CRUDAlbumRepository;
import de.whitefallen.tutorial.repository.mongo.MongoAlbumRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/example")
public class ExampleController {

    Logger logger = LoggerFactory.getLogger(ExampleController.class);

    @Autowired
    public MongoAlbumRepository mongoRepository;
    @Autowired
    public CRUDAlbumRepository repository;


    @GetMapping("/crud")
    public void testAlbum () {
        repository.save(new H2Album("Swan Songs", "Hollywood Undead"));
        repository.save(new H2Album("F8", "Five Finger Death Punch"));
        repository.save(new H2Album("Five", "Hollywood Undead"));
        repository.save(new H2Album("Day of the Dead", "Hollywood Undead"));
        repository.save(new H2Album("And Justice for None", "Five Finger Death Punch"));
        repository.save(new H2Album("MONOMANIA", "The Word Alive"));

        // fetch all customers
        logger.info("Albums found with findAll():");
        logger.info("-------------------------------");
        for (H2Album album : repository.findAll()) logger.info(album.toString());
        logger.info("");

        // fetch an individual customer by ID
        H2Album album = repository.findById(1L);
        logger.info("Albums found with findById(1L):");
        logger.info("--------------------------------");
        logger.info(album.toString());
        logger.info("");

        // fetch customers by last name
        logger.info("Albums found with findAlbumsByArtist('Hollywood Undead'):");
        logger.info("--------------------------------------------");
        repository.findAlbumsByArtist("Hollywood Undead").forEach(hu -> logger.info(hu.toString()));

        logger.info("");
    }

    @GetMapping("/mongo")
    public void mongoAlbum () {
        mongoRepository.save(new MongoAlbum("Swan Songs", "Hollywood Undead"));
        mongoRepository.save(new MongoAlbum("F8", "Five Finger Death Punch"));
        mongoRepository.save(new MongoAlbum("Five", "Hollywood Undead"));
        mongoRepository.save(new MongoAlbum("Day of the Dead", "Hollywood Undead"));
        mongoRepository.save(new MongoAlbum("And Justice for None", "Five Finger Death Punch"));
        mongoRepository.save(new MongoAlbum("MONOMANIA", "The Word Alive"));
    }
}
