package de.whitefallen.tutorial.entity.h2;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class H2Album {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String albumName;
    private String artist;

    protected H2Album() { }

    public H2Album(String _albumName, String _artist) {
        this.albumName = _albumName;
        this.artist = _artist;
    }

    @Override
    public String toString() {
        return String.format("Album[id=%d, albumName='%s', artist='%s']", id, albumName, artist);
    }

    public Long getId() {
        return this.id;
    }

    public String getAlbumName() {
        return this.albumName;
    }

    public String getArtist() {
        return this.artist;
    }
}
