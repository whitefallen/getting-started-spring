package de.whitefallen.tutorial.entity.mongo;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class MongoAlbum {

    @Id
    private String id;
    private String albumName;
    private String artist;

    protected MongoAlbum() { }

    public MongoAlbum(String _albumName, String _artist) {
        this.albumName = _albumName;
        this.artist = _artist;
    }

    @Override
    public String toString() {
        return String.format("Album[id='%s', albumName='%s', artist='%s']", id, albumName, artist);
    }

    public String getId() {
        return this.id;
    }

    public String getAlbumName() {
        return this.albumName;
    }

    public String getArtist() {
        return this.artist;
    }
}
