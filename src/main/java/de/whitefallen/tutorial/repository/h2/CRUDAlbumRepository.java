package de.whitefallen.tutorial.repository.h2;


import de.whitefallen.tutorial.entity.h2.H2Album;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CRUDAlbumRepository extends CrudRepository<H2Album, Long> {
    List<H2Album> findAlbumsByAlbumName(String albumName);
    List<H2Album> findAlbumsByArtist(String artist);

    H2Album findById(long id);
}
