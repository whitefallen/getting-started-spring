package de.whitefallen.tutorial.repository.mongo;


import de.whitefallen.tutorial.entity.mongo.MongoAlbum;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoAlbumRepository extends MongoRepository<MongoAlbum, String> {
    public List<MongoAlbum> findByArtist(String artistName);
    public MongoAlbum findByAlbumName(String album);
}
