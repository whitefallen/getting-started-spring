package de.whitefallen.tutorial;

import de.whitefallen.tutorial.repository.h2.CRUDAlbumRepository;
import de.whitefallen.tutorial.repository.mongo.MongoAlbumRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@EnableMongoRepositories(basePackageClasses={MongoAlbumRepository.class})
@EnableJpaRepositories(basePackageClasses={CRUDAlbumRepository.class})
@SpringBootApplication
public class TutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialApplication.class, args);
	}

}
